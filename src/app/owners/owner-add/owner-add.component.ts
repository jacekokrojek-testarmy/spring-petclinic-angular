import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {OwnerService} from '../owner.service';
import {Owner} from '../owner';
import {Router} from '@angular/router';

@Component({
  selector: 'app-owner-add',
  templateUrl: './owner-add.component.html',
  styleUrls: ['./owner-add.component.css']
})
export class OwnerAddComponent implements OnInit {

  errorMessage: string;

  owner = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    address: new FormControl(''),
    city: new FormControl(''),
    telephone: new FormControl(''),
  });

  ngOnInit() {
  }

  constructor(private ownerService: OwnerService, private router: Router) {
  }

  onSubmit() {
    console.warn(this.owner.value);
    this.owner.value.id = null;
    this.ownerService.addOwner(this.owner.value).subscribe(
      new_owner => {
        this.gotoOwnersList();
      },
      error => this.errorMessage = <any>error
    );
  }

  gotoOwnersList() {
    this.router.navigate(['/owners']);
  }

}
