import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { OwnerAddComponent } from './owner-add.component';
import { Router } from '@angular/router';
import Spy = jasmine.Spy;
import { OwnerService } from '../owner.service';
import { RouterStub } from '../../testing/router-stubs';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Owner } from '../owner';

class OwnerServiceStub {
  getOwners(): Observable<Owner[]> {
    return of();
  }
}

describe('OwnerAddComponent', () => {
  let component: OwnerAddComponent;
  let fixture: ComponentFixture<OwnerAddComponent>;
  let ownerService = new OwnerServiceStub();
  let spy: Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OwnerAddComponent],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: OwnerService, useClass: OwnerServiceStub },
        { provide: Router, useClass: RouterStub },
        { provide: ComponentFixtureAutoDetect, useValue: true }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerAddComponent);
    component = fixture.componentInstance;
    ownerService = fixture.debugElement.injector.get(OwnerService);
    spy = spyOn(ownerService, 'getOwners');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should present warning if firstname is too short', () => {
    const ownerAddComponentDe: DebugElement = fixture.debugElement;
    const ownerAddComponentEl: HTMLElement = fixture.nativeElement; // convenience method for fixture.debugElement.nativeElement.
    const inputs = ownerAddComponentEl.querySelectorAll('input');
    const fistNameInputIdx = 1;

    inputs[fistNameInputIdx].value = 'J';
    inputs[fistNameInputIdx].dispatchEvent(new Event('input'));

    const spanDe = ownerAddComponentDe.query(By.css('span.help-block'));
    const span: HTMLElement = spanDe.nativeElement;
    expect(span.textContent).toEqual('First name must be at least 2 characters long');
  });

});
