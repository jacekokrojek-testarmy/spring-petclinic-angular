/*
 *
 *  * Copyright 2016-2017 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */


/**
 * @author Vitaliy Fedoriv
 */

import {Component, OnInit, Input} from '@angular/core';
import {Pet} from '../pet';
import {PetService} from '../pet.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Owner} from '../../owners/owner';
import {PetType} from '../../pettypes/pettype';
import {PetTypeService} from '../../pettypes/pettype.service';
import {FileUploader, FileSelectDirective} from 'ng2-file-upload/ng2-file-upload';

import * as moment from 'moment';

const URL = 'http://localhost:9966/petclinic/api/';
const NO_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAZlBMVEX///9oa' +
  'GhjY2NeXl5hYWFaWlrOzs5fX19YWFienp55eXnk5OT7+/uDg4OTk5P19fXq6uq/v7+ZmZmNjY3U1NS3t7fIyMimpqbc3Nxra2uvr6' +
  '+FhYWqqqrZ2dnw8PDQ0NBzc3NQUFCOwUAsAAAOMElEQVR4nO1d6dKquhLdZhBBUJnE+eO8/0teHNAkpJMOBPRWsarOn7P9IIt0ekqn8+/fjBkzZsyYM' +
  'WPGjBkzZsyYMcMT4t11X56qbJOfkyQ555uwKtO/w+3b4/KBW1FmUcApDRgjIhijlPPFuUoP228Psi/i6ylvuDGyMKChSvlikx6+PVpnHE6JjZzIs6F5LtffH' +
  'jQefxnDs/uw5CQsvj10DIoNp67s3iwp3/w4yV3FetNrSbLqd8V1GfFh9F4kebT/NhUd4pIMnD4BlJ3ibxNSsL1w5oveA4xXv+QQbCsv4imDrMKf8QUuI/B7cqx+Q' +
  'lbTwCqfhAWNi8bJsY7uqBsZ5Lxx5KwfhtHy2/T+HWpq5tZQi8Jyf9jJ07FtnPFTFjVEiZFncPyugYw3KzO78+nPrDB2S5t/x/MvLsc9bB+Ii6e5Ls8GR4jwdFQWML' +
  'ZnSEAJDbI/RyVRhLA7RJOvTCM4gQHP+q2da0iBZxL+BS9nw4GxDHK59glgefjG28hx2B21AyGrbKjbvAv13hFZ7LyMHIm9VoUyfvGxXuIT03JcTSipoU5CGfXnLZda' +
  'jjz09XwbkkAnn349rJNuPQaJz1eAuOmWIM99RwJxplkJ5DhBwLHWKPSgHiNXto669pYEoycArt0lSPhY7nGqEVU+cuLxrys6NBpPcm5JdxpXo7riGiuxGje+KTVv/B' +
  'vvdV2C7Di2HV4vOpI6HsWuiE7iS+UdSR1LUIsOwdU0Yc2p++JR1M1afQ9hU6Vul13hGWFx3FRZIfV0Qdu648Qx7y+PO684+36FCX+qo0iOvl8RKRqNThqvrTXejWcf' +
  'NVPEhE7m5t+x1sUygdchpMoraOXz6Tbs9OkEn5kNVY1OTBBKeK38KVTlydOKKEiwga93bGQtE2S+HoyBiSDxpO728jIgk5oJE0FfS3GrEKx9PBQLM8GGog/Dn8gySq' +
  'bc8LIR9GIV9/JLxnAIQVgJNlpvsPO/VQhOmV1HEPQgp7kko2xKO4EiuCD5sLcUkpqZVMuABInsQfJh4fBRftiE5REwwUQxX4OijFIKWoavajxggpEaBwQDcmGx9JopT' +
  'b1hBu//rIhWfwNWSWrGi3XFwUJQCQVI70BgKz1nQhkFCbLWwF8kOV311Q+ZNIWRr/FbYSeoyCnpGQvcpClcTVYTiSGo7J/0nMRQnMK+n+k16PWhKJb7fSpiv98vl0UD' +
  '2RFEEVRcEdLLE5FjiiFqZhut6B1BELAWwQOP/015LXBEElQlrM/wTuJiZpf+BOPuzoMC8vl+WIKKsmGnHgOTtBUdEDNV9srT9xrAE1SMNXUflxQ12T/RtdxEzVxF+WWvr' +
  'nqMA72KzQR1cWApfjnqHvRIKWC71/Dfq/iOkJUi0Jpsbhe0cCaofDpnYyblKIl9FVLwx3tN5UYH7O5bahO/j0fqhy9pCu5qzSSHDaGpPoNjig9VYgrA746XK8F/MVee4A' +
  'RxWJikXX68l/02/7FI8e5OKIYbd4KKxXbUNYUo40gB2O7Wu1t3wVaYInCSwwRr8I3SUqJue9+SSwq/AgMUw0UN/sr0djEP6JgeFj9OYA8qbrt1g0ODtYzmf+QohiCI6ZD' +
  'QUhI1F4KykNpMxYU0SxDE0JMKhLMQTGGK43QS09Bh9uOj39MyOpJg0Y40UJfgQNJRllxWODrBBVx8cZA8LzxBSa/ZtPAE/F4egQ7SXOCNvmikrXMPaXmvAANAUVODn6GL' +
  's/BntvW7nYYhlNm+CmLqsE3jooNv0zAEM5m97IUYDViTpM4M72fxifmwkwtD0dxSbDFYKixDq2y7SClhnEZZdTpdwsaPpQ5KGN5/cRpsC3Hf3qqfYjRDtjqnoule7/ETC' +
  'es7Ue+jHTfx0XaPHcmQklIN/lFx1WvscGwkLkSCIxgLWSzE7hxvg/sHdKNrVp7urNDVcLLv/bzXAw2ZMHEhIjOCop+AkOzoieSJqIsk2ZQax7JT7PjmxumxjpLzHfcH1g' +
  '2O8DjE+JNeUQzFtWtz2frjBk0ec6yMF6MEhttbEd2E0fZEO+WU70G6VgGJ5gqZ+xY9msB97BbE6yK95AtQRN03Kd29moXzX6BRhI0ZpMxg73tsWUfigFF/4T7rSKQLazjM' +
  'eqwKMeOC2g4W5RrjJGShHtUd4eeN1yMiNVxvNptMemL2hEGFiGYVVdAkGgu6tP9+RWCsPrsBodH6faRGD1PBp5hyRnmmYnYH8wcmn6Z+/0p3ZtEBpnzv1XFKJHOIMRYGhp' +
  '9zZnCyUBidiaFhDsWsKcogSmKNWLgww4/mVyv9dT8+Hv7BfqqJoRjdoPYRJYOP+D3M8D2FaqW/BsHdAYZdcaNSF5U/ZvdCStDZfw5HT+/9hqU9/Hh6MoZnmbJFYlYJY96k' +
  'qu4hDIM2nEDsAb9CmAySZiND8XcYn0+MRhAeBsywFfELguFrxV5BZ87EUKitQdViim5pPYBh+zlRSYB2hxKyKUaGdfdTGZG4MQTzNK2QpphIvtXyG0BMjQwFxxTlSPti2M' +
  'bbNfDvMoPE/DmM6+tbDF9a6obz1l5iCpU1GBmKUoph6LgOIYbtmFCFCu/UOrRo0Qwx69BRl4IMX97FBZcybCs4oHVoYuiqSx3tIcTQpjtUCq+PHwH/bGKI/V0L0eoi6hsguWq' +
  '9/ASZ9n2Jy1n/c+PIhXWA8mkc/VKQYeHG8PUxgV1/I0NXv1TMPyIyrDaGgNh5ZOgcW6RuSQGQ4WvfEbut8XLye0ipc3woJQXsOWSI4culQXk0d9TPx0GaxqAjXdMukvcb2Gsa' +
  'IYZPeQFT9xAFyFoYGDrnaXZuYg3a6LsnuUXye78JfJqBoXOuTXwJpkQFWmhHXbMHEK/1cIC8NgPD0DVfKgkKoi4V9LxjTfccmOHzYaDnbWCYuCUllL9AGERQWR4dEoitsACq1' +
  'MjQfd9CmnV7OtFLLcarwBMOp2GGPfaepP1DezWcD4bttwcDEYM97LF/KJoLhDL1wbA9cgQ6QAaGPfaARS8IEW95YNgeLz7A2USYYY99fLkabgqGbRgK6RkjQ8m+4AjKZUbWer' +
  '/hDFszrennZ2fYq56mdCozGszwXTtqyFnBY+9VE3VwqWsbzJC32sGUOYYZ9qprkwZttfnDGH6a5x5MHhDM0Gmsb4hejbWiBl/Xphn46u34bo0eEMiwcJK3N0qX3RwkQxKVR6U' +
  'HMuHRW6xifetlK0PRAXOoEZbqvG0lNTiG7O7Dr0/RvcP8AwFn4WfZxJZNYjDI6Xu4SzrZZxFTFMOgFaD4WoZ5lORVKg7nZjtmCjG89qhMfMDlGAOGYWA2VIU1EwANQkx9OhX' +
  '/FA7V0wiGlo+EKESBniAdm3CqMpQWojlZg5lDYmiFXeiULJKhFIs4nXuSZ98Y6OOqoAnUrv0a4f5ez1AqaXMrasSfP8TWeZPVphPabNMaeUWNfo2t+wupcobUtIzwtfqEsmz' +
  '/zoVtr2WCv0FJz1AqbHA49fQA+hyw05mZxgpyEp3PSW29NxDBUOq94nwOWDqAauqo0OPMjPN5EoChtDXpfJZbLtQyxMETnQrSMJSPcrs3l8H2VACai/pmqJFB6ZB4j54K2L4' +
  'YqJYJYzCU+2K46pk7pDI6eCVOw5B395SkhiK9epvIhg7MYjkc7emPbmcTpYqlX7svyWBAHoPmsoT2L+Dj3W4I6EqjZ6QKiJ7dvuRWX/qzxp1G7R9+l7+/pRfs/zTyI6dWe7U' +
  'Y+qe0nVjUml8YGuaM3NxNysv1PjSh9Gvr5ghu0BpkAxtSWiG3O+w7hWr/o84+1A1yTUYnKLsZar8YByh9ExW3Yfs1gkpl/JAmVkrvS0lOb5B3Of6dAqX05Yf0vlSbTIp990' +
  'AR9X0arIudrMCH3ZJQKF7n+x9AJTM+QbW358BDoHIV1nuz2bErl1ds9EPqCyXApU//D3RGJyCo3NMwvKtqKpN5XNMD93Qan6CygeOjq6pSPsl3XyWo9Bf38sZOEA8e8pmA4' +
  'D9lA8dP51/1ghKwImQCgqpAeWoRj+vWpYnhvGOjbM95c55+hWCoqnBvTwajwGkJVgpBn62N1aX4EwT9XmZtOQX6DYLM83U3xpO8U2jRTCHYIwVsRufetYkJnjs2yvttMHBNu' +
  'vev2UW3kIGO0HQF8tUmILjuBNvjdIjXdz2a4N6S7vWnY13Tqc3+1qPfjpR1hGeFOTfSC5rLcpF1x/2xqzuGasxL1jUU6bi3sKWa+4BHvZGpe+fpgh3HuxdCe6fzaCL6RPfK3' +
  'OadY12HWGoqNca9tPoO3bkdthjjtYda8yqHLqy9oa2y47lvC3zbaOwvWUxy41QcaWthDYVdPd5RrTTfkUVTXdyn+7wLRvvsNGsRn7SlUnzCyzM1OvzJ0UdiaHvR10pNdAv4C' +
  '11P8cmRh0M1wTrTyed9CU52WdETcfdO9+dAdH088dhHQCkfH33HrosUGAsJeIY7YKWiyDiQqSST3iz5xi0Cdy9oELpayCJkYM9BOnZVAAhoGh8k+bnErpx1mnO4pSKZ8lZCF' +
  'dvckIQjjPLksjSfrt5eyzzgpkJMnn9rAp8ozKd9STOXPMpO++taHmd8OyzL6kxsZab02G9N+0Rpv8GCsIA2TINF/WgsXB/J/VIaYx/TJ9g3BfQDvYflAWR1mfJ6ZRO24QgcC' +
  'Q+/uwBlbCt8VToKjFe/xO+O+ATbM1cQSspfkU8JoMvlyG+Y4zcudhUZOJGEsmrKy817oHEue5NsjGc2ehrGB4qQGL0UPTvGibMz+0Wsy8ThUMzDv0N7sb+DQ7l5OGXGncc7O' +
  'ZKX6GPmP4ftNQ2Tx6VljEkNulnjyHHOkrAsJrywfTTEu8MyLasw2+R5fs7zTVid0v1195M2b8aMGTNmzJgxY8aMGTNmzPj/xP8A1aaw+syB7ecAAAAASUVORK5CYII=';

@Component({
  selector: 'app-pet-edit',
  templateUrl: './pet-edit.component.html',
  styleUrls: ['./pet-edit.component.css']
})
export class PetEditComponent implements OnInit {
  pet: Pet;
  imageurl: string;
  @Input() current_type: PetType;
  current_owner: Owner;
  pet_types: PetType[];
  errorMessage: string;

  public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});

  constructor(private petService: PetService, private petTypeService: PetTypeService, private router: Router, private route: ActivatedRoute) {
    this.pet = <Pet>{};
    this.current_owner = <Owner>{};
    this.current_type = <PetType>{};
    this.pet_types = [];
    this.imageurl = '';

  }

  ngOnInit() {

    this.petTypeService.getPetTypes().subscribe(
      pettypes => this.pet_types = pettypes,
      error => this.errorMessage = <any> error);

    const petId = this.route.snapshot.params['id'];
    this.petService.getPetById(petId).subscribe(
      pet => {
        this.pet = pet;
        this.current_owner = this.pet.owner;
        this.current_type = this.pet.type;
        this.imageurl = pet.hasPhoto ? URL + 'downloadFile/' + petId + '?d=' + Date.now() : NO_IMAGE;
      },
      error => this.errorMessage = <any> error);
    this.uploader.onBeforeUploadItem = function (item) {
      item.url = URL + 'uploadPhoto/' + petId;
    };
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
      this.imageurl = URL + 'downloadFile/' + petId + '?d=' + Date.now();
      console.log(this.imageurl);
    };
  }

  onSubmit(pet: Pet) {
    pet.type = this.current_type;
    let that = this;
    // format output from datepicker to short string yyyy/mm/dd format
    pet.birthDate = moment(pet.birthDate).format('YYYY/MM/DD');

    this.petService.updatePet(pet.id.toString(), pet).subscribe(
      res => this.gotoOwnerDetail(this.current_owner),
      error => this.errorMessage = <any> error
    );
  }

  gotoOwnerDetail(owner: Owner) {
    this.router.navigate(['/owners', owner.id]);
  }

}
